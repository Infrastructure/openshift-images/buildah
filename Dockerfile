FROM registry.fedoraproject.org/fedora:35

ENV STORAGE_DRIVER='vfs'
ENV BUILDAH_FORMAT='docker'
ENV BUILDAH_ISOLATION='chroot'

RUN dnf install -y buildah skopeo jq
RUN sed -i '/^mountopt =.*/d' /etc/containers/storage.conf && \
    echo 'cgroup_manager = "cgroupfs"' >> /etc/containers/libpod.conf && \
    echo 'events_logger = "file"' >> /etc/containers/libpod.conf
